%% Perceptron++
%% Copyright (C) 2021 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

MSE = csvread("MSE.csv");
MSE2 = csvread("MSE2.csv");
MSE3 = csvread("MSE3.csv");
figure;
hold on;
plot(10*log10(MSE), "DisplayName", "MSE Single Layer");
plot(10*log10(MSE2), "DisplayName", "MSE Two Layers");
plot(10*log10(MSE3), "DisplayName", "MSE Three Layers");

legend show;
xlabel("Sample");
ylabel("MSE (dB)");

print -dpng MSE.png
