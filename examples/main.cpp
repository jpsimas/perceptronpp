// Perceptron++
// Copyright (C) 2021 João Pedro de O. Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Example: Training a MLP to receive QAM symbols

#include "Perceptron.hh"
#include "BiasedPerceptron.hh"
#include "MultiLayerPerceptron.hh"
#include <iostream>
#include <fstream>
#include <complex>
#include <type_traits>
#include <random>

namespace detail {
  template <typename T>
  struct realTypeStruct {
    using type = T;
  };

  template <typename T>
  struct realTypeStruct<std::complex<T>> {
    using type = T;
  };
  
  template <typename T>
  using realType = realTypeStruct<T>::type;
}

template <size_t N_STEPS, class C, typename scalar_t, typename real_t = detail::realType<scalar_t>>
auto berCount(const C& ML, const std::array<scalar_t, N_STEPS>& x, const std::array<size_t, N_STEPS>& symNums) {
 std::array<size_t, N_STEPS> detected;
  size_t errCount = 0;
  //verification
  for(size_t i = 0; i < N_STEPS; i++) {
    const auto z = ML.eval(x[i]);
    real_t min = std::numeric_limits<real_t>::max();
    size_t minJ = 0;
    for(auto j = 0; j < z.rows(); j++) {
      const auto r = std::abs(z[j] - 1.0f);
      if(r < min) {
	min = r;
	minJ = j;
      }
    }
    detected[i] = minJ;
    if(detected[i] != symNums[i])
      errCount++;
  }
  return std::pair(detected, real_t(errCount)/N_STEPS);
}

int main() {
  using scalar_t = std::complex<float>;
  using real_t = detail::realType<scalar_t>;
  constexpr size_t N = 1;
  constexpr size_t M = 16;

  constexpr size_t M2 = 16;

  constexpr size_t M3 = 16;
  
  constexpr float eta = 5e-3;
  constexpr float eta2 = eta;
  constexpr float eta3 = eta;

  //initial w
  const Eigen::Matrix<scalar_t, N, M> w0 = Eigen::Matrix<scalar_t, N, M>::Random();
  const Eigen::Matrix<scalar_t, M, M2> w02 = Eigen::Matrix<scalar_t, M, M2>::Random(); 
  const Eigen::Matrix<scalar_t, M2, M3> w03 = Eigen::Matrix<scalar_t, M2, M3>::Random(); 
  
  BiasedPerceptron<scalar_t, N, M> p0(eta, w0);

  MultiLayerPerceptron<BiasedPerceptron<scalar_t, N, M>,
		       BiasedPerceptron<scalar_t, M, M2>>
    ML2(std::make_tuple(std::make_tuple(eta2, w0),
			std::make_tuple(eta2, w02)));
  
  MultiLayerPerceptron<BiasedPerceptron<scalar_t, N, M>,
		       BiasedPerceptron<scalar_t, M, M2>,
		       BiasedPerceptron<scalar_t, M2, M3>>
    ML3(std::make_tuple(std::make_tuple(eta3, w0),
		       std::make_tuple(eta3, w02),
		       std::make_tuple(eta3, w03)));

  constexpr size_t N_STEPS = 50000;

  const auto [x, symNums] = [] () {
    //generate P^2-QAM symbols
    constexpr real_t VAR_N = 0;
    constexpr real_t STD_N = std::sqrt(VAR_N);
    constexpr size_t P = 4;
    constexpr size_t dictionarySize = P*P;
    
    std::array<scalar_t, N_STEPS> ret;
    std::array<size_t, N_STEPS> symNums;
    
    std::default_random_engine gen;
    std::uniform_int_distribution<size_t> distInt(0, dictionarySize - 1);
    std::normal_distribution<real_t> distNorm;
    
    for(size_t i = 0; i < N_STEPS; i++) {
      symNums[i] = distInt(gen);
      ret[i] =
	// std::polar<real_t>(1.0f, 2*M_PI*(real_t(symNums[i]) - real_t((dictionarySize - 1)/2))/real_t(dictionarySize))
	scalar_t(real_t(symNums[i]/P) - (P - 1)/2.0f, real_t(symNums[i]%P) - (P - 1)/2.0f)/std::sqrt((dictionarySize - 1)/6.0f)
       	+ (STD_N/std::sqrt(2.0f))*scalar_t(distNorm(gen));
    }

    return std::make_pair(ret, symNums);
  }();

  std::array<scalar_t, N_STEPS> zVect;
  
  std::array<real_t, N_STEPS> MSE;
  std::array<real_t, N_STEPS> MSE2;
  std::array<real_t, N_STEPS> MSE3;


  // const Eigen::Matrix<scalar_t, M, 1> k1 = Eigen::Matrix<scalar_t, M, 1>::Random();
  // const Eigen::Matrix<scalar_t, M2, 1> k2 = Eigen::Matrix<scalar_t, M, 1>::Random();
  // const Eigen::Matrix<scalar_t, M3, 1> k3 = Eigen::Matrix<scalar_t, M, 1>::Random();
  
  for(size_t i = 0; i < N_STEPS; i++) {

    //reference
    const Eigen::Matrix<scalar_t, M, 1> d = [M] (size_t symNum) {
      Eigen::Matrix<scalar_t, M, 1> ret;
      for(size_t i = 0; i < M; i++)
	ret[i] = (symNum%M == i)? 1.0f : -1.0f;
      return ret;
    } (symNums[i]);
    
    const Eigen::Matrix<scalar_t, M2, 1> d2 = [M2] (size_t symNum) {
      Eigen::Matrix<scalar_t, M2, 1> ret;
      for(size_t i = 0; i < M2; i++)
	ret[i] = (symNum%M2 == i)? 1.0f : -1.0f;
      return ret;
    } (symNums[i]);
    
    const Eigen::Matrix<scalar_t, M3, 1> d3 = [M3] (size_t symNum) {
      Eigen::Matrix<scalar_t, M3, 1> ret;
      for(size_t i = 0; i < M3; i++)
	ret[i] = (symNum%M3 == i)? 1.0f : -1.0f;
      return ret;
    } (symNums[i]);

    // const Eigen::Matrix<scalar_t, M, 1> d = k1*x[i];
    // const Eigen::Matrix<scalar_t, M2, 1> d2 = k2*x[i];
    // const Eigen::Matrix<scalar_t, M3, 1> d3 = k3*x[i];
    
    auto [z, e] = p0.update(x[i], d);
    MSE[i] = e.squaredNorm();

    auto [z2, e2] = ML2.update(x[i], d2);
    MSE2[i] = e2.squaredNorm();
    
    auto [z3, e3] = ML3.update(x[i], d3);
    MSE3[i] = e3.squaredNorm();
    zVect[i] = z[0];
  }

  //verification
  const auto [detected1, BER1] = berCount(p0, x, symNums);
  const auto [detected2, BER2] = berCount(ML2, x, symNums);
  const auto [detected3, BER3] = berCount(ML3, x, symNums);

  std::cout << "BER1: " << BER1 << std::endl;
  std::cout << "BER2: " << BER2 << std::endl;
  std::cout << "BER3: " << BER3 << std::endl;
  
  //debugging info
  const auto b = p0.getBias();
  std::cout << "b: " << b << std::endl;

  // std::cout << "k1: " << k1 << std::endl;
  
  const auto w = p0.getWeights();
  std::cout << "w: " << w << std::endl;

  const auto b2_0 = ML2.getNode<0>().getBias();
  std::cout << "b2_0: " << b2_0 << std::endl;

  //export data to files
  std::ofstream file;
  file.open("MSE.csv");
  for(auto& x : MSE)
    file << x << std::endl;
  file.close();

  file.open("MSE2.csv");
  for(auto& x : MSE2)
    file << x << std::endl;
  file.close();

  file.open("MSE3.csv");
  for(auto& x : MSE3)
    file << x << std::endl;
  file.close();

  file.open("x.csv");
  for(auto& xi : x)
    file << xi.real() << " + " << xi.imag() << "j" << std::endl;
  file.close();
  
  file.open("symNums.csv");
  for(auto& x : symNums)
    file << x << std::endl;
  file.close();

  file.open("z.csv");
  for(auto& zi : zVect)
    file << zi << std::endl;
  file.close();

  // file.open("detected1.csv");
  // for(auto& detectedi : detected1)
  //   file << detectedi << std::endl;
  // file.close();

  // file.open("detected2.csv");
  // for(auto& detectedi : detected2)
  //   file << detectedi << std::endl;
  // file.close();

  // file.open("detected3.csv");
  // for(auto& detectedi : detected3)
  //   file << detectedi << std::endl;
  // file.close();
  
  return 0;
}
