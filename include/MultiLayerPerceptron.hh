// Perceptron++
// Copyright (C) 2021 João Pedro de O. Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MULTILAYERPERCEPTRON_HH
#define MULTILAYERPERCEPTRON_HH

#include "ANNBase.hh"
#include "MultiLayerPerceptronDetail.hh"

template<class... C>
class MultiLayerPerceptron : ANNBase<typename std::tuple_element_t<0, std::tuple<C...>>::scalar_t,
				     std::tuple_element_t<0, std::tuple<C...>>::inSize,
				     std::tuple_element_t<(sizeof...(C)) - 1, std::tuple<C...>>::outSize> {
protected:
  using Base = ANNBase<typename std::tuple_element_t<0, std::tuple<C...>>::scalar_t,
		       std::tuple_element_t<0, std::tuple<C...>>::inSize,
		       std::tuple_element_t<(sizeof...(C)) - 1, std::tuple<C...>>::outSize>;
  
  std::tuple<C...> nodes;
  static constexpr size_t L = std::tuple_size<decltype(nodes)>::value;
  static constexpr size_t N = std::tuple_element_t<0, decltype(nodes)>::inSize;
  static constexpr size_t M = std::tuple_element_t<L-1, decltype(nodes)>::outSize;
public:

  using VectIn = Base::VectIn;
  using VectOut = Base::VectOut;
  using Base::eval;
  using Base::update;
  
  //Auxiliar constructor to make contructor that takes array of tuples
  template<size_t... Is, typename... Args>
  MultiLayerPerceptron(std::index_sequence<Is...>,
		       std::tuple<Args...> args) :
    nodes(std::make_tuple(std::make_from_tuple<C>(std::get<Is>(args))...)) {
  }
    
  //Constructor that takes array of tuples arguments and constructs each node with each argument tuple
  template<typename... Args>
  MultiLayerPerceptron(std::tuple<Args...> args) :
    MultiLayerPerceptron(std::make_index_sequence<std::tuple_size<decltype(args)>::value>{}, args) {
  }

  constexpr
  std::pair<VectOut, VectOut>
  update(const VectIn& x, const VectOut& d){
    const auto [delta, ret] = mlpdetail::updateDetail<L - 1, C...>(nodes, x, d);
    return ret;
  }

  constexpr
  std::pair<VectOut, VectOut>
  update(const std::pair<VectIn, VectOut>& inputs) {
    return update(inputs.first, inputs.second);
  }

  constexpr
  VectOut
  eval(const VectIn& inputs) const {
    return mlpdetail::evalDetail<L - 1, C...>(nodes, inputs);
  }

  template<size_t i>
  const std::tuple_element_t<i, std::tuple<C...>>&
  getNode() const {return std::get<i>(nodes);}
  
};

#endif
