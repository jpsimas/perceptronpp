// Perceptron++
// Copyright (C) 2021 João Pedro de O. Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MULTILAYERPERCEPTRONDETAIL_HH
#define MULTILAYERPERCEPTRONDETAIL_HH
#include "ANNBase.hh"

namespace mlpdetail {
  //---------------Eval implementatation---------------

  template<size_t i, class... C>
  struct evalDetailStruct {

    using In_t = typename std::tuple_element_t<0, std::tuple<C...>>::VectIn;
    using Out_t = typename std::tuple_element_t<i, std::tuple<C...>>::VectOut;
    
    static
    Out_t
    evalDetail(const std::tuple<C...>& nodes, const In_t& inputs){
      auto& node = std::get<i>(nodes);
      using nextNode_t = std::tuple_element_t<i - 1, std::tuple<C...>>;

      static_assert(std::is_convertible<Out_t, typename nextNode_t::VectIn>::value);
      
      return node.eval(evalDetailStruct<i - 1, C...>::evalDetail(nodes, inputs));
    }
  };

  template<class... C>
  struct evalDetailStruct<0, C...> {
    
    using In_t = typename std::tuple_element_t<0, std::tuple<C...>>::VectIn;
    using Out_t = typename std::tuple_element_t<0, std::tuple<C...>>::VectOut;
    
    static
    Out_t
    evalDetail(const std::tuple<C...>& nodes, const In_t& inputs){
      return std::get<0>(nodes).eval(inputs);
    }
  };
  
  template<size_t i, class... C,
	   typename In_t = typename std::tuple_element_t<0, std::tuple<C...>>::VectIn,
	   typename Out_t = typename std::tuple_element_t<i, std::tuple<C...>>::VectOut>
  constexpr
  Out_t
  evalDetail(const std::tuple<C...>& nodes, const In_t& inputs) {
    return evalDetailStruct<i, C...>::evalDetail(nodes, inputs);
  }

  //---------------Update implementation---------------

  template<size_t i, class... C>
  struct updateDetailStruct {
    using Node_t = typename std::tuple_element_t<(sizeof...(C) - 1) - i, std::tuple<C...>>;
    using LastNode_t = typename std::tuple_element_t<(sizeof...(C) - 1), std::tuple<C...>>;
    using X_t = typename Node_t::VectIn;
    using D_t = typename LastNode_t::VectOut;

    using Ret_t = std::pair<typename LastNode_t::VectOut, typename LastNode_t::VectOut>;
    using Out_t = std::pair<typename Node_t::VectIn, Ret_t>;
    
    static
    constexpr
    Out_t
    updateDetail(std::tuple<C...>& nodes, const X_t& x, const D_t& dLast){
      auto& node = std::get<(sizeof...(C) - 1) - i>(nodes);
      const auto [y, z] = node.evalYZ(x);
      const auto [delta, ret] = updateDetailStruct<i - 1, C...>::updateDetail(nodes, z, dLast);
      return std::make_pair(node.update(x, y, z, delta), ret);
    }
  };

  template<class... C>
  struct updateDetailStruct<0, C...> {

    static constexpr size_t i = 0;
    using Node_t = typename std::tuple_element_t<(sizeof...(C) - 1) - i, std::tuple<C...>>;
    using LastNode_t = typename std::tuple_element_t<(sizeof...(C) - 1), std::tuple<C...>>;
    using X_t = typename Node_t::VectIn;
    using D_t = typename LastNode_t::VectOut;
    
    using Ret_t = std::pair<typename LastNode_t::VectOut, typename LastNode_t::VectOut>;
    using Out_t = std::pair<typename Node_t::VectIn, Ret_t>;
    
    static
    constexpr
    Out_t
    updateDetail(std::tuple<C...>& nodes, const X_t& x, const D_t& dLast){
      auto& node = std::get<(sizeof...(C) - 1) - i>(nodes);
      const auto [y, z] = node.evalYZ(x);
      const auto e = dLast - z;

      const auto delta = node.update(x, y, z, e.conjugate());
      
      return std::make_pair(delta, std::make_pair(z, e));
    }
  };

  template<size_t i, class... C,
	   typename Node_t = typename std::tuple_element_t<(sizeof...(C) - 1) - i, std::tuple<C...>>,
	   typename LastNode_t = typename std::tuple_element_t<(sizeof...(C) - 1), std::tuple<C...>>,
	   typename X_t = typename Node_t::VectIn,
	   typename D_t = typename LastNode_t::VectOut,
	   typename Ret_t = std::pair<typename LastNode_t::VectOut, typename LastNode_t::VectOut>,
	   typename Out_t = std::pair<typename Node_t::VectIn, Ret_t>>
  constexpr
  Out_t
  updateDetail(std::tuple<C...>& nodes, const X_t& x, const D_t& d) {
    return updateDetailStruct<i, C...>::updateDetail(nodes, x, d);
  }  
}

#endif
