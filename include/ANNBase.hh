// Perceptron++
// Copyright (C) 2021 João Pedro de O. Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ANNBASE_HH
#define ANNBASE_HH

#include <eigen3/Eigen/Dense>

template<typename T, size_t N, size_t M>
class ANNBase {
public:
  static constexpr size_t inSize = N;
  static constexpr size_t outSize = M;
  using scalar_t = T;

  using VectIn = Eigen::Matrix<T, N, 1>;
  using VectOut = Eigen::Matrix<T, M, 1>;
  
  virtual
  VectOut
  eval(const VectIn& inputs) const = 0;

  virtual
  std::pair<VectOut, VectOut>//output and error
  update (const VectIn& x, const VectOut& d) = 0;

  //extra overloads for N == 1

  template <size_t P = N, std::enable_if_t<(P == 1), bool> = true>
  constexpr
  VectOut
  eval(const T input) const {
    return eval(VectIn::Constant(input));
  }
  
  template <size_t P = N, std::enable_if_t<(P == 1), bool> = true>
  constexpr
  std::pair<VectOut, VectOut>//output and error
  update(const T x, const VectOut& d){
    return update(VectIn::Constant(x), d);
  }
};

#endif
