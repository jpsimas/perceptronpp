// Perceptron++
// Copyright (C) 2021 João Pedro de O. Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef BIASEDPERCEPTRON_HH
#define BIASEDPERCEPTRON_HH

#include <eigen3/Eigen/Dense>
#include "PerceptronBase.hh"

//Class that implments a MIMO perceptron/perceptron layer
//and the backpropagation algorithm given an activation
//function and it's derivative.
template <typename T, size_t N, size_t M,
	  //activation function
	  T (*activation) (const T) = PerceptronDetail::tanh,
	  //derivative of the activation function
	  //second argument is the value of the function at the point
	  //to be used if it makes computing the derivative easier.
	  T (*activationDer) (const T, const T) = PerceptronDetail::tanhDer>
class BiasedPerceptron :
  public PerceptronBase<T, N, M, activation, activationDer> {
protected:
  using Base = PerceptronBase<T, N, M, activation, activationDer>;
public:
  using VectIn = Base::VectIn;
  using VectOut = Base::VectOut;
  using VectW = Base::VectW;

protected:
  using Base::w;
  using Base::eta;
  VectOut b;
  
public:

  using Base::inSize;
  using Base::outSize;
  using Base::update;
  
  BiasedPerceptron(T eta,
		   const VectW& w0 = VectW::Zero(),
		   const VectOut& b0 = VectOut::Zero()) :
    Base(eta, w0),
    b(b0) {};

  VectOut getBias() const {return b;};
  
  std::pair<VectOut, VectOut>
  evalYZ(const VectIn& inputs) const {
    const auto y = w.adjoint()*inputs + b.conjugate();
    const auto z = y.unaryExpr(activation);
    return std::make_pair(y, z);
  }

  VectIn
  update(const VectIn& x,
	 const VectOut& y,
	 const VectOut& z,
	 const VectOut& deltaPrev) {
    
    const VectOut a = [&]() {
      VectOut ret;
      for(auto i = 0; i < ret.rows(); i++)
	ret[i] = activationDer(y[i], z[i]);
      return ret;
    }();
    
    const VectOut zeta = (a.array()*deltaPrev.array()).matrix();
    const decltype(w) grad = x*zeta.transpose();
    w += eta*grad;
    b += eta*zeta;
    const VectIn delta = w.conjugate()*zeta;
    return delta;
  }
  
};

#endif
