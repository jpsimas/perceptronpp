// Perceptron++
// Copyright (C) 2021 João Pedro de O. Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef PERCEPTRONBASE_HH
#define PERCEPTRONBASE_HH

#include <eigen3/Eigen/Dense>
#include "ANNBase.hh"

namespace PerceptronDetail {
  //logistic function
  template <typename T>
  constexpr T logistic(const T x) {
    return (std::tanh(x/T(2)) + T(1))/T(2);
  }

  template <typename T>
  constexpr std::complex<T> logistic(const std::complex<T> x) {
    return std::complex<T>(logistic<T>(x.real()), logistic<T>(x.imag()));
  }

  template <typename T>
  constexpr T logisticDer(const T x, const T f) {
    return (T(1) - f)*f;
  }

  template <typename T>
  constexpr std::complex<T> logisticDer(const std::complex<T> x, const std::complex<T> f) {
    //Wirtinger Derivative
    return 0.5f*logisticDer<T>(x.real(), f.real()) + logisticDer<T>(x.imag(), f.imag());
  }

  //tanh
  
  template <typename T>
  constexpr T tanh(const T x) {
    return std::tanh(x);
  }
  
  template <typename T>
  constexpr std::complex<T> tanh(const std::complex<T> x) {
    return std::complex<T>(tanh<T>(x.real()), tanh<T>(x.imag()));
  }
  
  template <typename T>
  constexpr T tanhDer(const T x, const T f) {
    return T(1) - f*f;
  }

  template <typename T>
  constexpr std::complex<T> tanhDer(const std::complex<T> x, const std::complex<T> f) {
    //Wirtinger Derivative
    return 0.5f*tanhDer<T>(x.real(), f.real()) + tanhDer<T>(x.imag(), f.imag());
  }

  // identity
  template <typename T>
  constexpr T identity(const T x) {
    return x;
  }

  template <typename T>
  constexpr T identityDer(const T x, const T f) {
    return 1;
  }
  
}

//Class that implments a MIMO perceptron/perceptron layer
//and the backpropagation algorithm given an activation
//function and it's derivative.
template <typename T, size_t N, size_t M,
	  //activation function
	  T (*activation) (const T) = PerceptronDetail::tanh,
	  //derivative of the activation function
	  //second argument is the value of the function at the point
	  //to be used if it makes computing the derivative easier.
	  T (*activationDer) (const T, const T) = PerceptronDetail::tanhDer>
class PerceptronBase : public ANNBase<T, N, M> {
protected:
  using Base = ANNBase<T, N, M>;
public:
  using VectIn = Base::VectIn;
  using VectOut = Base::VectOut;
  using VectW = Eigen::Matrix<T,N,M>;

protected:
  VectW w;//weights
  const T eta;
  
public:

  using Base::inSize;
  using Base::outSize;
  using Base::eval;
  using Base::update;
  
  PerceptronBase(T eta,
	     const VectW& w0 = VectW::Zero()) :
    w(w0),
    eta(eta) {};

  VectW getWeights() const {return w;};
  
  virtual
  std::pair<VectOut, VectOut>
  evalYZ (const VectIn& inputs) const = 0;

  constexpr
  VectOut
  eval(const VectIn& inputs) const {
    return evalYZ(inputs).second;
  }

  virtual
  VectIn
  update (const VectIn& x,
	  const VectOut& y,
	  const VectOut& z,
	  const VectOut& deltaPrev) = 0;
  
  constexpr
  std::pair<VectOut, VectOut>
  update (const VectIn& x,
	  const VectOut& d) {

    const auto [y, z] = evalYZ(x);
    const auto e = d - z;
    update(x, y, z, e.conjugate());
    return std::make_pair(z, e);
  }
  
};

#endif
